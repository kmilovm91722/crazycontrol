#CrazyControl

Este sistema emula el comportamiento de un interpretador de comandos con una pequeña
diferencia con los interpretadores normales: los programas a ejecutar se registran antes
de ejecutarse y luego se ejecutan como un grupo de trabajo que serán controlados a través 
de este sistema.

#Requerimientos para compilar

* ANTLR Version 3.3
* ANTLR Bibliotecas de C para ANTLR == 3.2.*
* Compilador de gcc >= 4.6.3
* GNU Make >= 3.82
* readline >= 6 
* uthash >= 1.9.8

#Variables definidas para compilar

* CLASSPATH=<Directorio intalado anltr>/antlr-3.3-complete.jar:<otros rutas para otros jars>

#Variables definidas para ejecutar:

Si instalan la biblioteca de funciones esta deberá quedar en el directorio 
(por omisión): /usr/local/lib, entonces definir:

* export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

#Compilar

Seguir la siguiente secuencia de comandos en bash para compilar despúes de
cumplir con los requerimientos listados anteriormente:

	$ cd ruta_directorio_raiz_crazycontrol/src/Linux/
	$ make
	$ sudo make install
	$ make main

#Ejecutar

	$ ./main
	
#Autores:

Daniel Camilo Vergara Marín - kmilovm91722@gmail.com
Anderson Lopez Monsalve - anderson1564@gmail.com

Sistemas Operativos - 2013-1 - Universidad Eafit - Profesor Juan Francisco Cardona McCormick