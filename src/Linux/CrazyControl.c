/* Autor: Daniel Camilo Vergara Marin
 * Autor: Anderson Lopez Monsalve
 *
 * Fecha: 05/06/2013
 * Practica final de Sistemas Operativos
 * Universidad Eafit
 * Profesor: Juan Francisco Cardona McCormick
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include "GramAbsComandos.h"
#include "CrazyControl.h"
#include <signal.h>
#include <fcntl.h>
#include <pthread.h>

pSt_process main_pendant_hash = NULL;
pSt_program main_program_hash = NULL;

int current_id_pendant;
int current_id_program;

int process_size;

/*To be called once at the beginning of the main*/
void crazy_control_init()
{
    current_id_program = 0;
    current_id_pendant = 0;
    process_size = 0;
    process_size += (int)sizeof(int);
    process_size += (int)(sizeof(char) * (MAX_SIZE_DATA * 2));
    process_size += (int)sizeof(UT_hash_handle);
}

int create(char *prog, char *arg)
{
    pSt_process p = (pSt_process)malloc(sizeof(main_pendant_hash));
    p->prog = (char *)malloc(sizeof(char) * MAX_SIZE_DATA);
    p->arg = (char *)malloc(sizeof(char) * MAX_SIZE_DATA);
    p->id = current_id_pendant++;
    p->prog = prog;
    p->arg = arg;
    HASH_ADD_INT(main_pendant_hash, id, p);
    printf("t%d\n", p->id);
    return p->id;
}

void delete(pNodoLista cablista)
{
    while (cablista)
    {
        int id = ((pID) cablista->elem)->id;
        pSt_process p;
        HASH_FIND_INT(main_pendant_hash, &id, p);
        if (p)
        {
            HASH_DEL(main_pendant_hash, p);
            printf("El proceso t%d ha sido borrado\n ", id);
        }
        else
        {
            printf("No se ha podido encontrar el proceso t%d\n ", id);
        }
        cablista = cablista->sig;
    }
}

void print_process_info(pSt_process p)
{
    printf("***************************************\n");
    printf("Proceso con id: t%d\n", p->id);
    printf("Program path: %s, Arguments: %s\n",
           p->prog, p->arg);
    printf("***************************************\n");
}

void print_program_info(pSt_program p)
{
    printf("##############################################################\n");
    printf("Programa con id: p%d y con los siguientes procesos:\n",
           p->id_program);
    pSt_process process_info_hash = p->process_info_hash;
    pSt_process temp;
    for (temp = process_info_hash; temp != NULL; temp = temp->hh.next)
    {
        print_process_info(temp);
    }
    printf("Estado: %s\n", get_state_name(p->state));
    printf("##############################################################\n");
}

void list(pNodoLista cablista)
{
    if (cablista)
    {
        while (cablista)
        {
            pID pid = (pID) cablista->elem;
            int id = pid->id;
            switch (pid->etiqueta)
            {
            case ID_PROG:
            {
                pSt_program program_ptr;
                HASH_FIND_INT(main_program_hash, &id, program_ptr);
                if (program_ptr)
                {
                    print_program_info(program_ptr);
                }
                else
                    printf("No se ha podido encontrar el programa p%d\n", id);
                break;
            }
            case ID_PEN:
            {
                pSt_process process_ptr;
                HASH_FIND_INT(main_pendant_hash, &id, process_ptr);
                if (process_ptr)
                    print_process_info(process_ptr);
                else
                    printf("No se ha podido encontrar el proceso t%d\n", id);
                break;
            }
            }
            cablista = cablista->sig;
        }
    }
    else
    {
        /*Then the list is empty, so let's show everything*/
        /*Show main_pendant_hash*/
        if (main_pendant_hash)
        {
            pSt_process p;
            for (p = main_pendant_hash; p != NULL; p = p->hh.next)
                print_process_info(p);
        }
        else
            printf("No existen procesos pendientes\n");
        /*Show main_program_hash*/
        if (main_program_hash)
        {
            pSt_program p;
            for (p = main_program_hash; p != NULL; p = p->hh.next)
                print_program_info(p);
        }
        else
            printf("No existen programas\n");
    }
}

void event(pNodoLista cablista)
{
    while (cablista)
    {
        pNodoLista pcablstprocs;
        pEvento pevento = (pEvento) cablista->elem;

        pcablstprocs = pevento->listaProgs->cab;
        int process_signal = pevento->evento;

        while (pcablstprocs)
        {
            int program_id = ((pID) pcablstprocs->elem)->id;
            pSt_program program_ptr;
            HASH_FIND_INT(main_program_hash, &program_id, program_ptr);
            if (program_ptr)
            {
                kill((program_ptr->parent_pid) * (-1), process_signal);
                printf("Se ha enviado una señal %d al programa p%d\n", process_signal, program_id);
            }
            else
            {
                printf("No se ha podido encontrar el programa p%d\n", program_id);
            }
            pcablstprocs = pcablstprocs->sig;
        }

        cablista = cablista->sig;
    }
}

void destroy(pNodoLista cablista)
{
    printf("Disparando...\n");
    printf("        \\___________n_   ___\n");
    printf("      /     __________| |___)   Muere maldito programa!!! BANG!!!!\n");
    printf("    /     /=/\n");
    printf("  /_____/\n");
    while (cablista)
    {
        int program_id = ((pID) cablista->elem)->id;
        pSt_program program_ptr;
        HASH_FIND_INT(main_program_hash, &program_id, program_ptr);
        if (program_ptr)
        {
            kill((program_ptr->parent_pid) * (-1), 9);
            printf("El programa p%d ha muerto\n", program_id);
        }
        else
        {
            printf("No se ha podido encontrar el programa p%d\n", program_id);
        }
        cablista = cablista->sig;
    }
}

char *get_state_name(process_state state)
{
    switch (state)
    {
    case PENDING:
        return "Pendiente";
    case EXECUTING:
        return "Ejecutandose";
    case SUSPENDED:
        return "Suspendido";
    case WAITING:
        return "Esperando";
    case FINISHED:
        return "Terminado";
    default:
        return "Estado desconocido";
    }
}

char **string_to_arg_array(char *args)
{
    /*We don't want to modify the argument, so, make a copy and cut the incoming coutes*/
    char *arg_copy = malloc(strlen(args - 1));
    memcpy(arg_copy, args + 1, strlen(args) - 2);

    char **res  = NULL;
    char   *p    = strtok (arg_copy, " ");
    int n_spaces = 0;

    /* split string and append tokens to 'res' */
    while (p)
    {
        res = realloc (res, sizeof (char *) * ++n_spaces);

        if (res == NULL)
            exit (-1); /* memory allocation failed */

        res[n_spaces - 1] = p;

        p = strtok (NULL, " ");
    }
    /* realloc one extra element for the last NULL */
    res = realloc (res, sizeof (char *) * (n_spaces + 1));
    res[n_spaces] = 0;
    return res;
}

void execute(pNodoLista pendant_cablista, char *input_file, char *output_file)
{
    pid_t temp_id;
    /*We need to iterate over the list two times, so make a copy of the pointer*/
    pNodoLista copy_cablista = (pNodoLista)malloc(sizeof(pNodoLista));
    copy_cablista = pendant_cablista;

    /*Process hash that is gonna be inside the new program that we are creating*/
    pSt_process new_process_hash = NULL;
    /*The new program that is gonna be stored in main_program_hash*/
    pSt_program new_program = (pSt_program)malloc(sizeof(program_hash));
    int process_count = 0;

    /*Copy all the pendant processes to the new_program process_hash from main_pendant_hash*/
    while (pendant_cablista)
    {
        /*Get  the pendant id from the list element*/
        pID pid = (pID) pendant_cablista->elem;
        int int_id = pid->id;
        /*Temporal process pointer that is gonna be inserted on the new_process_hash*/
        pSt_process temp_process;
        /*Get the pendant process from the global hash by id*/
        HASH_FIND_INT(main_pendant_hash, &int_id, temp_process);
        if (temp_process)
        {
            /*If we found the process, then make a copy and insert it in the hash*/
            pSt_process copy = (pSt_process)malloc(process_size);
            memcpy(copy, temp_process, process_size);
            /*Insert the process copy into the new_process_hash*/
            HASH_ADD_INT(new_process_hash, id, copy);
        }
        else
        {
            /*At this point, we didn't find that process,
              so, print the error and exit from this function*/
            fprintf(stderr,
                    "No se ha podido crear el programa porque t%d no existe\n ", int_id);
            return;
        }
        process_count++;
        pendant_cablista = pendant_cablista->sig;
    }

    /*Populate the new_program data*/
    new_program->id_program = current_id_program++;
    new_program->process_info_hash = new_process_hash;
    new_program->process_count = process_count;
    new_program->input_file = input_file;
    new_program->output_file = output_file;
    new_program->state = WAITING;

    /*Insert the new_program into main_program_hash*/
    HASH_ADD_INT(main_program_hash, id_program, new_program);
    printf("p%d\n", new_program->id_program);

    /**************************START EXECUTION*********************************/
    new_program->state = EXECUTING;

    if ((new_program->parent_pid = fork()) == 0)
    {
        /*Set the group pid of this parent process with his same pid*/
        setpgrp();
        /*Create the first pipe*/
        int fd1[2];
        if (pipe(fd1) < 0 )
        {
            /*Something went wrong*/
            fprintf(stderr, "Error: Debido a: %d %s\n", errno, strerror(errno));
            exit(1);
        }
        /*Create the second pipe*/
        int fd2[2];
        if (pipe(fd2) < 0 )
        {
            /*Something went wrong*/
            fprintf(stderr, "Error: Debido a: %d %s\n", errno, strerror(errno));
            exit(1);
        }

        int i = 0;
        /*Execute the processes*/
        while (i <= process_count + 1)
        {
            pSt_process temp_process;
            /*If it is not the last iteration*/
            if (i > 0 && i < new_program->process_count + 1)
            {
                pID pid = (pID) copy_cablista->elem;
                int process_id = pid->id;
                /*Get the process pointer from this program's process_hash*/
                HASH_FIND_INT(new_process_hash, &process_id, temp_process);
            }

            if ((temp_id = fork()) == 0)
            {
                /*Child code*/
                /*First iteration*/
                if (i == 0)
                {
                    close(fd1[1]);
                    close(fd1[0]);
                    /*Change the stdout for the second pipe*/
                    close(fd2[0]);
                    close(1);
                    dup2(fd2[1], 1);
                    char *arguments[] = {"cat", input_file};
                    execvp("/bin/cat", arguments);
                }
                /*Last iteration*/
                else if (i == new_program->process_count + 1)
                {
                    close(fd2[0]);
                    close(fd2[1]);
                    /*Change the stdin for the first pipe*/
                    close(fd1[1]);
                    close(0);
                    dup2(fd1[0], 0);

                    /*Change stdout for the output_file*/
                    freopen(output_file, "w", stdout);
                    
                    char *arguments[] = {"tee", NULL};
                    execvp("/usr/bin/tee", arguments);
                }
                /*Any other iteration*/
                else
                {
                    /*Change the stdin for the first pipe*/
                    close(fd1[1]);
                    close(0);
                    dup2(fd1[0], 0);

                    /*Change the stdout for the second pipe*/
                    close(fd2[0]);
                    close(1);
                    dup2(fd2[1], 1);
                    
                    char arguments[MAX_SIZE_DATA];
                    sprintf(arguments, "\"anyname %s", temp_process->arg + 1);
                    

                    char **splited_arg = string_to_arg_array(arguments);
                    /*char **splited_arg = string_to_arg_array(temp_process->arg);*/
                    
                    execvp(temp_process->prog, splited_arg);
                }
            }
            else
            {
                /*Parent code*/
                close(fd1[0]);
                close(fd1[1]);

                /*Only if we are on the last iteration, close the second pipe*/
                if (i == new_program->process_count + 1)
                {
                    close(fd2[0]);
                    close(fd2[1]);

                    int ret_val;
                    /*Wait for the last process*/
                    waitpid(temp_id, &ret_val, 0);
                    exit(0);
                }
                else
                {
                    /*Leave the second pipe open for the next process*/
                    /*Copy the second pipe on the first pipe and then create a new one*/
                    memcpy(fd1, fd2, sizeof(fd2));

                    if (pipe(fd2) < 0 )
                    {
                        /*Something went wrong*/
                        fprintf(stderr, "Error: Debido a: %d %s\n", errno, strerror(errno));
                        exit(1);
                    }
                }
            }
            /*If it is not the last iteration*/
            if ( i > 0 && i < new_program->process_count + 1)
                copy_cablista = copy_cablista->sig;
            i++;
        }
    }
    else
    {
        /*Main parent code*/
        /*Wait for the last process to finish*/
        int ret_val;
        
        waitpid(new_program->parent_pid, &ret_val, 0);
        fprintf(stdout, 
                "El programa p%d ha terminado su ejecucion con estado %d\n",
                 new_program->id_program,WEXITSTATUS(ret_val));
        /*When the last child finishes, remove this program from the main_program_hash*/
        HASH_DEL(main_program_hash, new_program);
    }
}

void *execute_on_thread (void *args)
{
    p_execute_args arg = args;
    execute(arg->cablista, arg->input_file, arg->output_file);
    return (void *)0;
}

void execute_from_main(void *args)
{
    p_execute_args arg = args;
    pthread_t thread;
    pthread_create(&thread,
                   NULL,
                   execute_on_thread,
                   arg);
}
