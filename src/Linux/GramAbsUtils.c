#include "GramAbsComandos.h"
#include <stdlib.h>
#include <strings.h>

pCadena
nuevaCadenaNB(int tam) {

  pCadena ret;

  ret = (pCadena) malloc(tam);

  if (ret) bzero((void *) ret, tam);

  return ret;
}

pCadena
nuevaCadenaCopiaANTLR3Str(pANTLR3_STRING str) {
  pCadena ret;

  ret = (pCadena) malloc(str->len+1);

  if (ret) {

    strcpy(ret, (char *) str->chars);
    *(ret + str->len) = '\0';
  }

  return ret;
}

pCadena
nuevaCadenaCopiaStr(char *str) {
  pCadena ret = NULL;

  if (str) {

    int tam;
    tam = strlen(str) + 1;
    ret = (pCadena) malloc(tam);

    if (ret) {
      strcpy(ret, str);
      *(ret + tam - 1) = '\0';
    }
  }

  return NULL;
}

void
borrarCadena(pCadena pcadena) {

  free(pcadena);
}

pLista 
nuevaListaVacia() {
  pLista ret;
  ret = (pLista) malloc(sizeof(Lista));
 
  if (ret) {
    ret->cab = NULL;
    ret->ult = NULL;
  }

  return ret;
}

static pNodoLista
nuevoNodoLista(void* elem) {
  pNodoLista ret;

  ret = (pNodoLista) malloc(sizeof(NodoLista));

  if (ret) {
    ret->elem = elem;
    ret->sig  = (pNodoLista) NULL;
  }

  return ret;
}

pLista
adicionarElemLista(pLista cab, void* elem) {
  pNodoLista nodo;

  nodo = nuevoNodoLista(elem);
  if (cab->ult) { cab->ult->sig = nodo; }
  cab->ult = nodo;
  if (!(cab->cab)) { cab->cab = nodo; }

  return cab;
}

