/* Autor: Juan Francisco Cardona McCormick
 * Fecha: 25/04/2013
 *
 * Historia de modificaciones.
 *  01/05/2013 - Completando las clases.
 */


#include "comandosLexer.h"
#include "comandosParser.h"
#include <stdio.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <pthread.h>
#include "CrazyControl.h"

const char *prompt = ">>> ";

int
main(int argc, char *argv[])
{
    crazy_control_init();
    char *linea;
    pANTLR3_INPUT_STREAM input;
    pcomandosLexer lexer;
    pcomandosParser parser;
    pANTLR3_COMMON_TOKEN_STREAM tokens;
    pANTLR3_INPUT_STREAM datosALexer;
    uint32_t tamanoBuffer;
    pANTLR3_UINT8 nombreDatos = (pANTLR3_UINT8) "Entrada estandar";
    pComandos ret;

    while ((linea = readline(prompt)))
    {

        tamanoBuffer = strlen(linea);

        input = antlr3NewAsciiStringCopyStream(linea,
                                               tamanoBuffer,
                                               nombreDatos);
        lexer = comandosLexerNew(input);
        tokens = antlr3CommonTokenStreamSourceNew(ANTLR3_SIZE_HINT,
                 TOKENSOURCE(lexer));
        parser = comandosParserNew(tokens);

        ret = parser->comandos(parser);

        if (!ret)
        {
            fprintf(stderr, "Comando no reconocido\n");
        }
        else
        {
            switch (ret->etiqueta)
            {
            case CMD_CREAR:
            {
                char *prog = (char *)malloc(sizeof(char) * 200);
                char *arg = (char *)malloc(sizeof(char) * 200);
                prog = ret->cmd.crear.prog;
                arg = ret->cmd.crear.arg;
                create(prog, arg);
            }
            break;

            case CMD_DESTRUIR:
            {
                pNodoLista cablista = ret->cmd.destruir.listaProgs->cab;
                destroy(cablista);
            }
            break;

            case CMD_LISTAR:
            {
                if (ret->cmd.listar.listaIds)
                {
                    pNodoLista cablista = ret->cmd.listar.listaIds->cab;
                    list(cablista);
                }
                else
                {
                    list(NULL);
                }
                break;
            }

            case CMD_BORRAR:
            {
                pNodoLista cablista = ret->cmd.borrar.listaPends->cab;
                delete(cablista);
                break;
            }

            case CMD_EJECUTAR:
            {
                p_execute_args args = (p_execute_args)malloc(sizeof(execute_args));

                args->cablista = (pNodoLista)ret->cmd.ejecutar.listaPends->cab;
                args->input_file = (char *)ret->cmd.ejecutar.fen;
                args->output_file = (char *)ret->cmd.ejecutar.fsal;
                execute_from_main(args);
                break;
            }

            case CMD_ENVIARSENAL:
            {
                pNodoLista cablista = ret->cmd.enviarsenal.listaEventos->cab;
                event(cablista);
                break;
            }
            default:
                printf("digite un comando\n");
                break;
            }

            /* parser->free(parser); */
            /* tokens->free(tokens); */
            /* lexer->free(lexer); */
        }
    }
    return 0;
}
