#ifndef CRAZYCONTROL_H
#define CRAZYCONTROL_H

#include "uthash.h"

#define SIZE_TEXT_FILE 512
#define FULL_SIZE_TEXT_FILE 5000
#define MAX_SIZE_DATA 200
#define EXECUTE_NUM_ARGS 3
#define TEST 0

enum states { PENDING,
              EXECUTING,
              SUSPENDED,
              WAITING,
              FINISHED
            };

typedef enum states process_state;

struct process_hash
{
    int id;
    char *prog;
    char *arg;
    UT_hash_handle hh;
};

typedef struct process_hash *pSt_process;
typedef struct process_hash process_hash;

struct program_hash
{
    int id_program;
    process_state state;
    pSt_process process_info_hash;
    int process_count;
    pid_t parent_pid;
    char *input_file;
    char *output_file;
    UT_hash_handle hh;
};

typedef struct program_hash *pSt_program;
typedef struct program_hash program_hash;

struct execute_args
{
    pNodoLista cablista;
    char *input_file;
    char *output_file;
};

typedef struct execute_args *p_execute_args;
typedef struct execute_args execute_args;

extern int current_id_pendant;
extern int current_id_program;

void crazy_control_init();
int create(char *, char *);
void delete(pNodoLista);
void print_process_info(pSt_process);
void list(pNodoLista);
void destroy(pNodoLista);
void execute(pNodoLista, char *, char *);
void *execute_on_thread (void *);
void execute_from_main(void *);
char *get_state_name(process_state);
void write_pipe_to_file(int, char *);
void read_file_to_pipe(int , char *);
void event(pNodoLista);
#endif
